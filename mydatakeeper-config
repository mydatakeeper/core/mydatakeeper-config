#!/usr/bin/python3
import logging
import os
import sys
import signal
from mydatakeeper import Parser
from mydatakeeper_config import Applications

from gi.repository.GLib import MainLoop


version = __import__('subprocess').check_output(["git", "describe", "--always"]).strip().decode('utf-8')

if __name__ == '__main__':
    logging.basicConfig(
        format=os.getenv("LOG_FORMAT", "[%(levelname).4s][%(name)s] - %(message)s"),
        level=os.getenv("LOG_LEVEL", logging.INFO))
    logger = logging.getLogger("fr.mydatakeeper.config")

    def getBus(param, value):
        if value == 'system':
            from pydbus import SystemBus
            return SystemBus()
        if value == 'session':
            from pydbus import SessionBus
            return SessionBus()
        return None

    logger.info("Parsing arguments")
    parser = Parser({
        'bus': {
            'longopt': 'bus',
            'required': True,
            'default': 'system',
            'callback': getBus,
            'description': "The type of bus used by Dbus interfaces. Either system or session",
        },
        'lib-dir': {
            'longopt': 'lib-dir',
            'required': True,
            'default': '/usr/lib/mydatakeeper-config',
            'callback': lambda o, a: os.path.realpath(a),
            'description': "The folder in which applications utilities are installed"
        },
        'var-dir': {
            'longopt': 'var-dir',
            'required': True,
            'default': '/var/lib/mydatakeeper-config',
            'callback': lambda o, a: os.path.realpath(a),
            'description': "The folder in which applications utilities are installed"
        },
    }, version)
    parser.run()

    # Initialization
    app_handler = Applications(
        parser.value('lib-dir'),
        parser.value('var-dir'),
    )

    # Main loop
    bus = parser.value('bus')
    app_handler.setup(bus)

    loop = MainLoop()

    logger.info("Start main loop")
    signal.signal(signal.SIGINT, lambda n, f: loop.quit())
    signal.signal(signal.SIGTERM, lambda n, f: loop.quit())
    signal.signal(signal.SIGHUP, lambda n, f: (
        logger.info("Reloading applications"),
        app_handler.teardown(),
        app_handler.setup(bus)
    ))
    loop.run()
    logger.info("Stop main loop")

    app_handler.teardown()
    logger.info("Exiting")
