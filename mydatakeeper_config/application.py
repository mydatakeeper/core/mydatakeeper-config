import base64
import io
import json
import logging
import mimetypes
import os
import threading

from pydbus import DBusException

from .applicationdbusinterface import ApplicationsDBusInterface, ApplicationDBusInterface
from .config import Config
from .interface import Interface

logger = logging.getLogger("fr.mydatakeeper.Application")

class ApplicationException(DBusException):
    silent = True
    def __init__(self, *parameters, **kwargs):
        Exception.__init__(self, *parameters, **kwargs)
        self.dbus_name = 'fr.mydatakeeper.Application.' + type(self).__name__

class LoadError(ApplicationException):
    pass

class UnloadError(ApplicationException):
    pass

class IconNotFoundError(ApplicationException):
    pass

class Applications(ApplicationsDBusInterface):
    def __init__(self, lib_dir, var_dir):
        ApplicationsDBusInterface.__init__(self)
        self.var_dir = var_dir
        self.lib_dir = lib_dir

    def setup(self, bus):
        logger.debug("Setting up application handler interfaces")
        ApplicationsDBusInterface.setup(self, bus)
        logger.debug("Application handler interfaces setup")

        logger.debug("Setting up application handler")
        app_dir = os.path.abspath(os.path.realpath("{}/apps/".format(self.lib_dir)))
        apps = os.listdir(app_dir)
        self.apps = dict()
        self.load_applications(
            lambda app_name: Application(
                app_name,
                os.path.abspath(os.path.realpath("{}/apps/{}/manifest.json".format(self.lib_dir, app_name))),
                os.path.abspath(os.path.realpath("{}/apps/{}/config.json".format(self.var_dir, app_name))),
                os.path.abspath(os.path.realpath("{}/apps/{}/html".format(self.lib_dir, app_name))),
            ),
            apps, bus)
        logger.debug("Application handler setup")

    def load_applications(self, callback, application_names, bus):
        for app_name in application_names:
            try:
                app = callback(app_name)
                app.setup(bus)
                self.apps[app_name] = app
            except ApplicationException as e:
                logger.error(e, exc_info=True)
            except Exception as e:
                logger.critical(e, exc_info=True)

    def teardown(self):
        logger.debug("Tearing down application handler")
        for app_name in self.apps:
            try:
                self.apps[app_name].teardown()
            except ApplicationException:
                pass
        del self.apps
        logger.debug("Application handler teardown")

        logger.debug("Tearing down application handler interfaces")
        ApplicationsDBusInterface.teardown(self)
        logger.debug("Application handler interfaces teardown")

    #
    # List method
    #
    def list(self):
        return dict((k, (v.has_config(), v.has_interface())) for (k, v) in self.apps.items())

    #
    # Reset method
    #
    def reset(self):
        for app in self.apps.values():
            app.reset()


class Application(ApplicationDBusInterface):
    _config = None
    _interface = None

    #
    # Setup/teardown functions
    #
    def __init__(self, app_name, manifest_file, config_file, interface_dir):
        ApplicationDBusInterface.__init__(self, app_name)
        self.app_name = app_name
        self.manifest_file = manifest_file
        self.config_file = config_file
        self.interface_dir = interface_dir

    def setup(self, bus):
        logger.debug("Loading '{}' manifest file".format(self.manifest_file))
        with open(self.manifest_file, 'r') as fd:
            self.manifest = json.load(fd)

        logger.debug("Setting up application '{}' interfaces".format(self.app_name))
        ApplicationDBusInterface.setup(self, bus)
        self.bus = bus
        logger.debug("Application '{}' interfaces setup".format(self.app_name))

        logger.debug("Setting up application '{}'".format(self.app_name))
        self.load()
        logger.debug("Application '{}' setup".format(self.app_name))

    def teardown(self):
        logger.debug("Tearing down application '{}'".format(self.app_name))
        self.unload()
        logger.debug("Application '{}' teardown".format(self.app_name))

        logger.debug("Stopping application interfaces")
        ApplicationDBusInterface.teardown(self)
        logger.debug("Application interfaces stopped")

    #
    # Loading/unloading method
    #
    def load(self):
        self.load_config()
        self.load_interface()

    def load_config(self):
        if 'fields' in self.manifest:
            config = Config(self.app_name, self.config_file, self.manifest)
            config.setup(self.bus)
            self._config = config

    def load_interface(self):
        if 'interface' in self.manifest:
            interface = Interface(self.app_name, self.interface_dir, self.manifest)
            interface.setup(self.bus)
            self._interface = interface

    def unload(self):
        self.unload_config()
        self.unload_interface()

    def unload_config(self):
        if self.has_config():
            self._config.teardown()
            self._config = None

    def unload_interface(self):
        if self.has_interface():
            self._interface.teardown()
            self._interface = None

    def has_config(self):
        return self._config is not None

    def has_interface(self):
        return self._interface is not None

    #
    # Reset method
    #
    def reset(self):
        if self.has_config():
            self._config.reset()

    #
    # GetIcon method
    #
    def get_icon(self, size):
        icons = self.manifest['icons']
        filename = icons.get(size, icons.get('*'))
        if filename is None:
            raise IconNotFoundError(size)
        filename = os.path.dirname(self.manifest_file) + '/' + filename

        mimetypes.init()
        mimetype = mimetypes.guess_type(filename)[0]

        with open(filename, 'rb') as fd:
            content = fd.read()
        content = base64.b64encode(content)

        return 'data:' + mimetype + ';base64,' + content.decode('utf-8')
