import logging

from mydatakeeper import to_variant, apply_signature

from .dbusinterface import DBusInterface

logger = logging.getLogger("fr.mydatakeeper.Application.Interface.DBus")

class ApplicationsDBusInterface(DBusInterface):
    dbus_name = "fr.mydatakeeper.Applications"
    dbus_path = "/fr/mydatakeeper/Applications"
    dbus_xml_path = "/usr/share/dbus-1/interfaces/fr.mydatakeeper.Applications.xml"

    def __init__(self):
        with open(self.dbus_xml_path, 'r') as fd:
            node_info = fd.read()
        DBusInterface.__init__(self, self.dbus_name, self.dbus_path, node_info)

    def setup(self, bus):
        DBusInterface.setup(self, bus)

    def teardown(self):
        DBusInterface.teardown(self)

    #
    # Listing functions
    #
    def List(self):
        """List of applications and hints about config/interface"""
        return self.list()

    def Reset(self):
        """Reset applications configurations"""
        self.reset()

class ApplicationDBusInterface(DBusInterface):
    dbus_name = "fr.mydatakeeper.Applications"
    dbus_xml_path = "/usr/share/dbus-1/interfaces/fr.mydatakeeper.Application.xml"

    def __init__(self, app_name):
        with open(self.dbus_xml_path, 'r') as fd:
            node_info = fd.read()
        DBusInterface.__init__(self, self.dbus_name, '/' + app_name.replace('.', '/'), node_info)

    def setup(self, bus):
        DBusInterface.setup(self, bus)

    def teardown(self):
        DBusInterface.teardown(self)

    #
    # Application Methods
    #
    def Reset(self):
        """Reset application configuration"""
        self.reset()

    def GetIcon(self, size):
        """
        Read icon data and return it
        """
        return self.get_icon(size)

    #
    # Manifest properties
    #
    @property
    def name(self):
        """
        """
        return self.manifest['name']

    @property
    def version(self):
        """
        """
        return self.manifest['version']

    @property
    def admin(self):
        """
        """
        return self.manifest.get('admin', False)

    @property
    def contact(self):
        """
        """
        return self.manifest['contact']

    @property
    def website(self):
        """
        """
        return self.manifest['website']

    @property
    def description(self):
        """
        """
        return self.manifest['description']

    @property
    def tags(self):
        """
        """
        return self.manifest['tags']

    @property
    def categories(self):
        """
        """
        return self.manifest['categories']

    @property
    def icons(self):
        """
        """
        return self.manifest['icons']


    @property
    def hasConfig(self):
        """
        """
        return self.has_config()

    @property
    def fields(self):
        """
        """
        return apply_signature('a{sa{sv}}', self.manifest.get('fields', {}))

    @fields.setter
    def fields(self, value):
        """
        """
        self.manifest['fields'] = value


    @property
    def hasInterface(self):
        """
        """
        return self.has_interface()

    @property
    def interface(self):
        """
        """
        return apply_signature('a{sv}', self.manifest.get('interface', {}))
