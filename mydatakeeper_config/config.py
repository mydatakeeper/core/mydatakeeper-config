import logging
import json
import os

from .configdbusinterface import ConfigDBusInterface

logger = logging.getLogger("fr.mydatakeeper.Config")

class ConfigException(Exception):
    pass

class Config(ConfigDBusInterface):
    #
    # Setup/teardown functions
    #
    def __init__(self, app_name, config_file, manifest):
        self.app_name = app_name
        self.config_file = config_file
        self.config = dict()
        self.manifest = manifest
        ConfigDBusInterface.__init__(self, app_name, manifest['fields'].keys())

    def setup(self, bus):
        logger.debug("Loading '{}' config file".format(self.app_name))
        if os.path.isfile(self.config_file):
            with open(self.config_file, 'r') as fd:
                self.config = json.load(fd)

        logger.debug("Setting up '{}' config interfaces".format(self.app_name))
        ConfigDBusInterface.setup(self, bus)
        logger.debug("'{}' config interfaces setup".format(self.app_name))

    def teardown(self):
        logger.debug("Tearing down '{}' config interfaces".format(self.app_name))
        ConfigDBusInterface.teardown(self)
        logger.debug("'{}' config interfaces teardown".format(self.app_name))

    def reset(self):
        logger.info("Resetting '{}' config".format(self.app_name))
        self.config = dict()
        self.properties_changed(dict(map(lambda n: (n, self.get(n)), self.field_names())), [])

    @property
    def fields(self):
        """
        Fields are retrieved from manifest to keep up to date fields
        in the event of a manifest update through dbus
        """
        return self.manifest['fields']

    def field_names(self):
        return self.fields.keys()

    def get(self, name):
        if name in self.config:
            return self.config[name]
        return self.fields[name]['default']

    def set(self, name, value):
        self.config[name] = value
        fd = None
        try:
            fd = open(self.config_file, 'w')
            json.dump(self.config, fd, indent=4)
        except:
            logger.warning("Cannot write configuration file '{}'".format(self.config_file))
        finally:
            if fd is not None:
                fd.close()

        self.properties_changed({name: value}, [])

    def type(self, name):
        return self.fields[name]['type']

    def properties_changed(self, changed_properties, invalidated_properties):
        logger.debug("Changing properties. Changed : '{}' Invalidated: '{}'".format(changed_properties, invalidated_properties))
        self.PropertiesChanged(changed_properties, invalidated_properties)
