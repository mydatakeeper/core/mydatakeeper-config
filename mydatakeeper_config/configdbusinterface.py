import logging

from pydbus import DBusException
from pydbus.generic import signal

from .dbusinterface import DBusInterface

logger = logging.getLogger("fr.mydatakeeper.Config.Interface.DBus")

class DbusConfigException(DBusException):
    silent = True
    def __init__(self, e):
        Exception.__init__(self, *e.args)
        self.dbus_name = 'fr.mydatakeeper.' + type(e).__name__

class ConfigDBusInterface(DBusInterface):
    dbus_name = "fr.mydatakeeper.Applications"
    __attrs__ = list()

    def __init__(self, app_name, attrs):
        self.__attrs__ = attrs
        properties = map(lambda field: """
            <property name="{0}" type="{1}" access="readwrite">
                <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
            </property>
        """.format(field, self.dbus_type(field)), self.__attrs__)

        node_info = """
        <node>
            <interface name='fr.mydatakeeper.Config'>
                {0}
            </interface>
        </node>
        """.format(''.join(properties))

        DBusInterface.__init__(self, self.dbus_name, '/' + app_name.replace('.', '/'), node_info)

    def dbus_type(self, field):
        return {
            'text': 's',
            'password': 's',
            'string': 's',
            'password-utf-8': 's',
            'password-utf8': 's',
            'utf-8': 's',
            'utf8': 's',
            'password-utf-8Extended': 's',
            'password-utf8Extended': 's',
            'utf-8Extended': 's',
            'utf8Extended': 's',
            'ascii': 's',
            'password-ascii': 's',
            'date': 't',
            'time': 't',
            'datetime': 't',
            'timestamp': 't',
            'ip': 's',
            'ipv4': 's',
            'ipv6': 's',
            'select': 's',
            'selectMultiple': 'as',
            'dict': 'a{ss}',
            'metadata': 'a(ss)',
            'boolean': 'b',
        }[self.type(field)]

    def setup(self, bus):
        DBusInterface.setup(self, bus)

    def teardown(self):
        DBusInterface.teardown(self)

    def __getattr__(self, attr):
        if attr in self.__attrs__:
            return self.get(attr)
        return DBusInterface.__getattr__(self, attr)

    def __setattr__(self, attr, value):
        if attr in self.__attrs__:
            return self.set(attr, value)
        return DBusInterface.__setattr__(self, attr, value)

    class properties_change_signal(signal):
        def emit(self, object, changed_properties, invalidated_properties):
            signal.emit(self, object, 'fr.mydatakeeper.Config', changed_properties, invalidated_properties)
    PropertiesChanged = properties_change_signal()
