import logging

logger = logging.getLogger("fr.mydatakeeper.Interface.DBus")

class DBusInterface:
    def static_setup(dbus, dbus_name, dbus_path):
        if dbus_name not in DBusInterface.names_owned:
            logger.info("Owning dbus name '{}'".format(dbus_name))
            DBusInterface.names_owned[dbus_name] = dbus.request_name(dbus_name)

        if dbus_path not in DBusInterface.object_registered:
            logger.info("Registering dbus path '{}'".format(dbus_path))
            DBusInterface.object_registered[dbus_path] = dbus_name

    def static_teardown(dbus_name, dbus_path):
        if dbus_path in DBusInterface.object_registered:
            logger.info("Unregistering dbus path '{}'".format(dbus_path))
            del DBusInterface.object_registered[dbus_path]

        if dbus_name not in DBusInterface.object_registered.values():
            logger.info("Unowning dbus name '{}'".format(dbus_name))
            DBusInterface.names_owned[dbus_name].unown()
            del DBusInterface.names_owned[dbus_name]

    def escape(char):
        if 'A' <= char and char <= 'Z':
            return char
        if 'a' <= char and char <= 'z':
            return char
        if '0' <= char and char <= '9':
            return char
        if '/' == char:
            return char
        return '_x'+str(ord(char))+'_'

    def __init__(self, dbus_name, dbus_path=None, node_info=None):
        self.dbus_name = dbus_name
        if dbus_path is None:
            dbus_path = dbus_path.replace('.', '/')
        self.dbus_path = ''.join(map(DBusInterface.escape, dbus_path))
        self.node_info = node_info

    def setup(self, dbus):
        DBusInterface.static_setup(dbus, self.dbus_name, self.dbus_path)
        self.object_registration = dbus.register_object(self.dbus_path, self, self.node_info)

    def teardown(self):
        self.object_registration.unregister()
        DBusInterface.static_teardown(self.dbus_name, self.dbus_path)

DBusInterface.object_registered = dict()
DBusInterface.names_owned = dict()
