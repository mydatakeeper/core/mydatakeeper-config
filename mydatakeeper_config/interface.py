import logging
import json
import os

from .interfacedbusinterface import InterfaceDBusInterface

logger = logging.getLogger("fr.mydatakeeper.Interface")

class InterfaceException(Exception):
    pass

class OutOfBoundFileError(Exception):
    pass

class IndexNotFoundError(Exception):
    pass

class FileNotFoundError(Exception):
    pass

class Interface(InterfaceDBusInterface):
    #
    # Setup/teardown functions
    #
    def __init__(self, app_name, interface_dir, manifest):
        self.app_name = app_name
        self.interface_dir = interface_dir
        self.manifest = manifest
        InterfaceDBusInterface.__init__(self, app_name)

    def setup(self, bus):
        logger.debug("Setting up '{}' interface interfaces".format(self.app_name))
        InterfaceDBusInterface.setup(self, bus)
        logger.debug("'{}' interface interfaces setup".format(self.app_name))

    def teardown(self):
        logger.debug("Tearing down '{}' interface interfaces".format(self.app_name))
        InterfaceDBusInterface.teardown(self)
        logger.debug("'{}' interface interfaces teardown".format(self.app_name))

    @property
    def root(self):
        """
        """
        return self.interface_dir

    @property
    def index(self):
        """
        """
        return self.manifest['interface'].get('index', 'index.html')

    @property
    def files(self):
        """
        """
        return self.manifest['interface'].get('files', list())

    @property
    def methods(self):
        """
        """
        return self.manifest['interface'].get('methods', dict())
