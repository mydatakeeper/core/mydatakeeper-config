import logging

from pydbus import DBusException
from mydatakeeper import to_variant, apply_signature

from .dbusinterface import DBusInterface

logger = logging.getLogger("fr.mydatakeeper.Interface.Interface.DBus")

class DbusInterfaceException(DBusException):
    silent = True
    def __init__(self, e):
        Exception.__init__(self, *e.args)
        self.dbus_name = 'fr.mydatakeeper.' + type(e).__name__

class InterfaceDBusInterface(DBusInterface):
    dbus_name = "fr.mydatakeeper.Applications"
    dbus_xml_path = "/usr/share/dbus-1/interfaces/fr.mydatakeeper.Interface.xml"

    def __init__(self, app_name):
        with open(self.dbus_xml_path, 'r') as fd:
            node_info = fd.read()
        DBusInterface.__init__(self, self.dbus_name, '/' + app_name.replace('.', '/'), node_info)

    def setup(self, bus):
        DBusInterface.setup(self, bus)

    def teardown(self):
        DBusInterface.teardown(self)
