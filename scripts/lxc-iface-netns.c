#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <lxc/lxccontainer.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

uid_t euid;
uid_t ruid;

char* name = NULL;
int pid = -1;

char* iface = NULL;
bool wlan = false;

void usage(const char* cmd) {
    fprintf(
        stderr,
        "Usage:\n"
        "    %s -n|--name=<name> -i|--iface=<iface>\n"
        "    %s -h|--help\n"
        "\n"
        "Options:\n"
        "    -n,--name=      Name of an LXC container\n"
        "    -i,--iface=     Name of a network interface\n",
        cmd, cmd
    );
}

void parse_args(int argc, char** argv) {
    static struct option long_options[] =
    {
        {"name", required_argument, NULL, 'n'},
        {"iface", required_argument, NULL, 'i'},
        {"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0}
    };

    int ch=0;
    while ((ch = getopt_long(argc, argv, "n:i:h", long_options, NULL)) != -1)
    {
        switch (ch){
            case 'n':
                name = strdup(optarg);
                break;
            case 'i':
                iface = strdup(optarg);
                break;
            case 'h':
                usage(argv[0]);
                exit(EXIT_SUCCESS);
            default:
                usage(argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (iface == NULL) {
        fprintf(stderr, "Missing parameter 'iface'\n");
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (name == NULL) {
        fprintf(stderr, "Missing parameter 'name'\n");
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }
}

void get_lxc_pid() {
    struct lxc_container *container;
    if ((container = lxc_container_new(name, NULL)) == NULL) {
        fprintf(stderr, "Fail to create container '%s'\n", name);
        exit(EXIT_FAILURE);
    }

    if (!container->is_defined(container)) {
        fprintf(stderr, "Container '%s' is not defined\n", name);
        lxc_container_put(container);
        exit(EXIT_FAILURE);
    }

    if (!container->is_running(container)) {
        fprintf(stderr, "Container '%s' is not running\n", name);
        lxc_container_put(container);
        exit(EXIT_FAILURE);
    }

    pid = container->init_pid(container);

    lxc_container_put(container);
}

int file_exist (const char *filename) {
    struct stat buffer;   
    return (stat (filename, &buffer) == 0);
}

void get_iface_type() {
    char filename[256] = {0};
    if (sprintf(filename, "/sys/class/net/%s", iface) < 0) {
        fprintf(stderr, "Unable to sprintf (line %d)\n", __LINE__);
        exit(EXIT_FAILURE);
    } else if (!file_exist(filename)) {
        fprintf(stderr, "Device '%s' does not exists\n", iface);
        exit(EXIT_FAILURE);
    }

    if (sprintf(filename, "/sys/class/net/%s/device", iface) < 0) {
        fprintf(stderr, "Unable to sprintf (line %d)\n", __LINE__);
        exit(EXIT_FAILURE);
    } else if (!file_exist(filename)) {
        fprintf(stderr, "Device '%s' is not physical\n", iface);
        exit(EXIT_FAILURE);
    }

    if (sprintf(filename, "/sys/class/net/%s/wireless", iface) < 0) {
        fprintf(stderr, "Unable to sprintf (line %d)\n", __LINE__);
        exit(EXIT_FAILURE);
    }

    if (!file_exist(filename)) {
        return;
    }

    wlan = true;
    if (sprintf(filename, "/sys/class/net/%s/phy80211/name", iface) < 0) {
        fprintf(stderr, "Unable to sprintf (line %d)\n", __LINE__);
        exit(EXIT_FAILURE);
    }

    FILE *fd;
    if ((fd = fopen(filename, "r")) == NULL) {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(EXIT_FAILURE);
    }

    if (fscanf(fd, "%s\n", iface) <= 0) {
        fprintf(stderr, "Unable to read file '%s'\n", filename);
        fclose(fd);
        exit(EXIT_FAILURE);
    }

    fclose(fd);
}

// TODO : See if this is really useful or delete it

// void safe_mkdir(const char* filename) {
//     int ret;
//     if ((ret = mkdir(filename, 0755)) < 0 && errno != EEXIST) {
//         fprintf(stderr, "Unable to create folder '%s'\n", filename);
//         exit(EXIT_FAILURE);
//     }
// }

// void bind_netns() {
//     safe_mkdir("/var/run/netns");

//     char target[256] = {0};
//     if (sprintf(target, "/var/run/netns/%s", name) < 0) {
//         fprintf(stderr, "Unable to sprintf (line %d)\n", __LINE__);
//         exit(EXIT_FAILURE);
//     }

//     if (file_exist(target)) {
//         return;
//     }

//     int fd;
//     if ((fd = open(target, O_RDWR|O_CREAT)) < 0) {
//         fprintf(stderr, "Unable to open file '%s'\n", target);
//         exit(EXIT_FAILURE);
//     }
//     close(fd);

//     char source[256] = {0};
//     if (sprintf(source, "/proc/%d/ns/net", pid) < 0) {
//         fprintf(stderr, "Unable to sprintf (line %d)\n", __LINE__);
//         exit(EXIT_FAILURE);
//     }

//     if (mount(source, target, "fuse", MS_BIND, NULL) < 0) {
//         fprintf(stderr, "Unable to mount %s onto %s\n", source, target);
//         exit(EXIT_FAILURE);
//     }
// }

void change_wlan_netns() {
    // TODO: check if 'iw' command is available

    pid_t fpid;
    fpid = fork();
    if (fpid < 0) {
        fprintf(stderr, "Unable to fork 'iw' child process\n");
        exit(EXIT_FAILURE);
    }

    if (fpid == 0) {
        char pidstr[30];
        sprintf(pidstr, "%d", pid);
        execlp("iw", "iw", "phy", iface, "set", "netns", pidstr, (char *)NULL);
        fprintf(stderr, "Unable to execute 'iw' child process\n");
        exit(EXIT_FAILURE);
    }

    int status;
    if (wait(&status) != fpid || WEXITSTATUS(status) != 0) {
        fprintf(stderr, "Child process 'iw' exited with error\n");
        exit(EXIT_FAILURE);
    }
}

void change_ethernet_netns() {
    // TODO: check if 'ip' command is available

    pid_t fpid;
    fpid = fork();
    if (fpid < 0) {
        fprintf(stderr, "Unable to fork 'ip' child process\n");
        exit(EXIT_FAILURE);
    }

    if (fpid == 0) {
        char pidstr[30];
        sprintf(pidstr, "%d", pid);
        execlp("ip", "ip", "link", "set", "dev", iface, "netns", pidstr, (char *)NULL);
        fprintf(stderr, "Unable to execute 'ip' child process\n");
        exit(EXIT_FAILURE);
    }

    int status;
    if (wait(&status) != fpid || WEXITSTATUS(status) != 0) {
        fprintf(stderr, "Child process 'ip' exited with error\n");
        exit(EXIT_FAILURE);
    }
}

void do_setuid()
{
    int status;

#ifdef _POSIX_SAVED_IDS
    status = seteuid (euid);
#else
    status = setreuid (ruid, euid);
#endif
    if (status < 0) {
        fprintf(stderr, "Unable to set EUID to %d : %s\n", euid, strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (geteuid() != euid) {
        fprintf(stderr, "An error occured while setting EUID to %d : %s\n", euid, strerror(errno));
        exit(EXIT_FAILURE);
    }

    printf("RUID set to %d\n", geteuid());
}

void undo_setuid()
{
    int status;

#ifdef _POSIX_SAVED_IDS
    status = seteuid (ruid);
#else
    status = setreuid (euid, ruid);
#endif
    if (status < 0) {
        fprintf(stderr, "Unable to set EUID to %d : %s\n", ruid, strerror(errno));
        exit(EXIT_SUCCESS);
    }

    if (geteuid() != ruid) {
        fprintf(stderr, "An error occured while setting EUID to %d : %s\n", euid, strerror(errno));
        exit(EXIT_FAILURE);
    }

    printf("EUID set to %d\n", geteuid());
}

int main(int argc, char** argv) {
    parse_args(argc, argv);

    ruid = getuid ();
    euid = geteuid ();
    printf("RUID = %d\n", ruid);
    printf("EUID = %d\n", euid);

    /* non root code */
    undo_setuid();

    get_lxc_pid();
    printf("LXC init PID is %d\n", pid);

    get_iface_type();
    printf("Physical interface is %s (%s)\n", iface, wlan ? "wlan" : "ethernet");

    /* root code */
    do_setuid();

    //bind_netns();
    wlan ? change_wlan_netns() : change_ethernet_netns();
    printf("Interface network namespace successfully changed\n");

    exit(EXIT_SUCCESS);
}