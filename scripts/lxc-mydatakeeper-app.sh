#!/bin/bash

# Make sure the usual locations are in PATH
export PATH=$PATH:/usr/sbin:/usr/bin:/sbin:/bin

usage()
{
    cat <<EOF
Usage:
    $1 -h|--help
    $1 -n|--name=<name> -p|--path=<path>
EOF
    return 0
}

install()
{
    path=$1
    mkdir -p "${path}/overlay"
}

configure()
{
    path=$1
    rootfs=$2
    name=$3

cat <<EOF > $path/config
# Automatically generated lxc container configuration file
# For mydatakeeper application %APP_ID%
# Do not edit manually (unless you know what you are doing).

# Architecture
lxc.arch = x86_64

# Hostname
lxc.uts.name = %APP_ID%

# Halt/Reboot/Stop signals
lxc.signal.halt = SIGPWR
lxc.signal.reboot = SIGINT
lxc.signal.stop = SIGKILL

# Init command
# TODO: use script that only return false
#lxc.execute.cmd = /usr/sbin/nologin
lxc.init.cmd = /sbin/init

# Proc
# TODO: Configure proc filesystem for the container.
lxc.proc.oom_score_adj = 1

# Ephemeral
lxc.ephemeral = 0

# Network
%APP_NETWORK%
# TODO: Generate network configuration for the container.
#lxc.net.[i].type =
#lxc.net.[i].flags =
#lxc.net.[i].link =
#lxc.net.[i].mtu =
#lxc.net.[i].name =
#lxc.net.[i].hwaddr =
#lxc.net.[i].ipv4.address =
#lxc.net.[i].ipv4.gateway =
#lxc.net.[i].ipv6.address =
#lxc.net.[i].ipv6.gateway =

# New pseudo-TTY instance
lxc.pty.max = 2

# Container system console
lxc.console.path = none

# Console through the TTYs
lxc.tty.max = 2

# Console device location
lxc.tty.dir =

# /dev directory
lxc.autodev = 1

# Mount points
lxc.mount.auto = cgroup:ro proc:mixed sys:ro
# TODO: mount(?) run & dev/shm
#lxc.mount.entry = run run tmpfs rw,nodev,relatime,mode=755 0 0
#lxc.mount.entry = shm dev/shm tmpfs rw,nodev,noexec,nosuid,relatime,mode=1777,create=dir 0 0

# Root file system
lxc.rootfs.path = overlay:%OVERLAY_DIR%:%ROOTFS_DIR%:%APP_DIR%/overlay

# Control group
lxc.cgroup.blkio =
lxc.cgroup.cpu =
lxc.cgroup.cpuset =
lxc.cgroup.dir =
lxc.cgroup.net_cls =
lxc.cgroup.net_prio =
lxc.cgroup.devices =

# Capabilities
lxc.cap.drop = audit_control
#lxc.cap.drop = audit_read
lxc.cap.drop = audit_write
#lxc.cap.drop = block_suspend ??
#lxc.cap.drop = chown
lxc.cap.drop = dac_override
lxc.cap.drop = dac_read_search
lxc.cap.drop = fowner
lxc.cap.drop = fsetid
#lxc.cap.drop = ipc_lock ??
lxc.cap.drop = ipc_owner
lxc.cap.drop = kill
#lxc.cap.drop = lease
#lxc.cap.drop = linux_immutable
lxc.cap.drop = mac_admin
lxc.cap.drop = mac_override
lxc.cap.drop = mknod
#lxc.cap.drop = net_admin
#lxc.cap.drop = net_bind_service
#lxc.cap.drop = net_broadcast
#lxc.cap.drop = net_raw
lxc.cap.drop = setfcap
#lxc.cap.drop = setgid
lxc.cap.drop = setpcap
#lxc.cap.drop = setuid
lxc.cap.drop = sys_admin
lxc.cap.drop = sys_boot
#lxc.cap.drop = sys_chroot
lxc.cap.drop = sys_module
lxc.cap.drop = sys_nice
lxc.cap.drop = sys_pacct
#lxc.cap.drop = sys_ptrace
lxc.cap.drop = sys_rawio
lxc.cap.drop = sys_resource
lxc.cap.drop = sys_time
lxc.cap.drop = sys_tty_config
lxc.cap.drop = syslog
lxc.cap.drop = wake_alarm

# Namespaces
# Do not keep any namespace, we want to create a new environment
#lxc.namespace.keep =
lxc.namespace.clone = cgroup ipc mnt net pid user uts
# TODO: add shared namespaces
%APP_SHARED_NS%
#lxc.namespace.share.[namespace identifier] =

# Resource limits
# TODO: set prlimits
#lxc.prlimit.[limit name] =

# Sysctl
# TODO: Set kernel parameters
#lxc.sysctl.[kernel parameters name] =

# AppArmor profile
# TODO: Set apparmor context
#lxc.apparmor.profile = %APP_ID%
lxc.apparmor.profile = lxc-container-default-cgns
lxc.apparmor.allow_incomplete = 0

# SELinux context
# TODO: Set SELinux context
#lxc.selinux.context =

# Seccomp configuration
# TODO: Set seccomp context
#lxc.seccomp.profile = %APP_DIR%/seccomp.conf
lxc.seccomp.profile = /usr/share/lxc/config/common.seccomp

# PR_SET_NO_NEW_PRIVS
lxc.no_new_privs = 1

# ID mapping
lxc.idmap = u 0 100000 %APP_UID%
lxc.idmap = g 0 100000 %APP_GID%

# Hooks
lxc.hook.version = 0

# Host namespace hooks (not required for now)
#lxc.hook.pre-start =
#lxc.hook.start-host =
#lxc.hook.stop =
#lxc.hook.post-stop =
#lxc.hook.clone =
#lxc.hook.destroy =

# Container namespace hooks
lxc.hook.mount = /usr/share/lxcfs/lxc.mount.hook
# TODO: decide wether container hooks should be run

# Logging
lxc.log.level = %APP_LOG_LEVEL%
lxc.log.file = %APP_DIR%/%APP_ID%.log
lxc.log.syslog = %APP_SYSLOG_LEVEL%

# Autostart
lxc.start.auto = %APP_START_AUTO%
lxc.start.delay = %APP_START_DELAY%
lxc.start.order = %APP_START_ORDER%
lxc.monitor.unshare = 0
#lxc.monitor.signal.pdeath = SIGKILL
lxc.group = %APP_GROUPS%

# Environment variables used to generate template LXC configuration file
lxc.environment = APP_ID=%APP_ID%
lxc.environment = APP_NETWORK=%APP_NETWORK%
lxc.environment = APP_SHARED_NS=%APP_SHARED_NS%
lxc.environment = APP_DEPENDENCIES=%APP_DEPENDENCIES%
lxc.environment = APP_CAPABILITIES=%APP_CAPABILITIES%
lxc.environment = APP_LOG_LEVEL=%APP_LOG_LEVEL%
lxc.environment = APP_SYSLOG_LEVEL=%APP_SYSLOG_LEVEL%
lxc.environment = APP_START_AUTO=%APP_START_AUTO%
lxc.environment = APP_START_DELAY=%APP_START_DELAY%
lxc.environment = APP_START_ORDER=%APP_START_ORDER%
lxc.environment = APP_GROUPS=%APP_GROUPS%
EOF

    # TODO: replace these varaibles with actual values
    sed \
        -e "s|%OVERLAY_DIR%|${overlay}|g" \
        -e "s|%ROOTFS_DIR%|${rootfs}|g" \
        -e "s|%APP_DIR%|${path}|g" \
        -e "s|%APP_ID%|${name}|g" \
        -e "s|%APP_NETWORK%|${network}|g" \
        -e "s|%APP_SHARED_NS%|${namespaces}|g" \
        -e "s|%APP_DEPENDENCIES%|${dependencies}|g" \
        -e "s|%APP_CAPABILITIES%|${capabilities}|g" \
        -e "s|%APP_UID%|${mapped_uid}|g" \
        -e "s|%APP_GID%|${mapped_gid}|g" \
        -e "s|%APP_LOG_LEVEL%|${log_level}|g" \
        -e "s|%APP_SYSLOG_LEVEL%|${syslog_level}|g" \
        -e "s|%APP_START_AUTO%|${APP_START_AUTO}|g" \
        -e "s|%APP_START_DELAY%|${APP_START_DELAY}|g" \
        -e "s|%APP_START_ORDER%|${APP_START_ORDER}|g" \
        -e "s|%APP_GROUPS%|${APP_GROUPS}|g" \
        -i "${path}/config"

    cat <<EOF > "${path}/seccomp.conf"
2
whitelist errno 13

[all]
_llseek
_newselect
accept
accept4
access
adjtimex
alarm
bind
brk
capget
capset
chdir
chmod
chown
chown32
clock_getres
clock_gettime
clock_nanosleep
close
connect
copy_file_range
creat
dup
dup2
dup3
epoll_create
epoll_create1
epoll_ctl
epoll_ctl_old
epoll_pwait
epoll_wait
epoll_wait_old
eventfd
eventfd2
execve
execveat
exit
exit_group
faccessat
fadvise64
fadvise64_64
fallocate
fanotify_mark
fchdir
fchmod
fchmodat
fchown
fchown32
fchownat
fcntl
fcntl64
fdatasync
fgetxattr
flistxattr
flock
fork
fremovexattr
fsetxattr
fstat
fstat64
fstatat64
fstatfs
fstatfs64
fsync
ftruncate
ftruncate64
futex
futimesat
get_robust_list
get_thread_area
getcpu
getcwd
getdents
getdents64
getegid
getegid32
geteuid
geteuid32
getgid
getgid32
getgroups
getgroups32
getitimer
getpeername
getpgid
getpgrp
getpid
getppid
getpriority
getrandom
getresgid
getresgid32
getresuid
getresuid32
getrlimit
getrusage
getsid
getsockname
getsockopt
gettid
gettimeofday
getuid
getuid32
getxattr
inotify_add_watch
inotify_init
inotify_init1
inotify_rm_watch
io_cancel
io_destroy
io_getevents
io_setup
io_submit
ioctl
ioprio_get
ioprio_set
ipc
kill
lchown
lchown32
lgetxattr
link
linkat
listen
listxattr
llistxattr
lremovexattr
lseek
lsetxattr
lstat
lstat64
madvise
memfd_create
mincore
mkdir
mkdirat
mknod
mknodat
mlock
mlock2
mlockall
mmap
mmap2
mprotect
mq_getsetattr
mq_notify
mq_open
mq_timedreceive
mq_timedsend
mq_unlink
mremap
msgctl
msgget
msgrcv
msgsnd
msync
munlock
munlockall
munmap
nanosleep
newfstatat
open
openat
pause
pipe
pipe2
poll
ppoll
prctl
pread64
preadv
preadv2
prlimit64
pselect6
pwrite64
pwritev
pwritev2
read
readahead
readlink
readlinkat
readv
recv
recvfrom
recvmmsg
recvmsg
remap_file_pages
removexattr
rename
renameat
renameat2
restart_syscall
rmdir
rt_sigaction
rt_sigpending
rt_sigprocmask
rt_sigqueueinfo
rt_sigreturn
rt_sigsuspend
rt_sigtimedwait
rt_tgsigqueueinfo
sched_get_priority_max
sched_get_priority_min
sched_getaffinity
sched_getattr
sched_getparam
sched_getscheduler
sched_rr_get_interval
sched_setaffinity
sched_setattr
sched_setparam
sched_setscheduler
sched_yield
seccomp
select
semctl
semget
semop
semtimedop
send
sendfile
sendfile64
sendmmsg
sendmsg
sendto
set_robust_list
set_thread_area
set_tid_address
setfsgid
setfsgid32
setfsuid
setfsuid32
setgid
setgid32
setgroups
setgroups32
setitimer
setpgid
setpriority
setregid
setregid32
setresgid
setresgid32
setresuid
setresuid32
setreuid
setreuid32
setrlimit
setsid
setsockopt
setuid
setuid32
setxattr
shmat
shmctl
shmdt
shmget
shutdown
sigaltstack
signalfd
signalfd4
sigreturn
socket
socketcall
socketpair
splice
stat
stat64
statfs
statfs64
statx
symlink
symlinkat
sync
sync_file_range
syncfs
sysinfo
tee
tgkill
time
timer_create
timer_delete
timer_getoverrun
timer_gettime
timer_settime
timerfd_create
timerfd_gettime
timerfd_settime
times
tkill
truncate
truncate64
ugetrlimit
umask
uname
unlink
unlinkat
utime
utimensat
utimes
vfork
vmsplice
wait4
waitid
waitpid
write
writev

personality allow [0,0,SCMP_CMP_EQ,0]
personality allow [0,8,SCMP_CMP_EQ,0]
personality allow [0,131072,SCMP_CMP_EQ,0]
personality allow [0,131080,SCMP_CMP_EQ,0]
personality allow [0,4294967295,SCMP_CMP_EQ,0]
EOF
}

name=
path=
rootfs=
overlay=
dependencies=
capabilities=
mapped_uid=
mapped_gid=
log_level=1
syslog_level=daemon

options=$(getopt -o n:p:r:h -l name:,path:,rootfs:,mapped-uid:,mapped-gid:,overlay:,help -- "$@")
if [ $? -ne 0 ]; then
        usage $(basename $0)
    exit 1
fi
eval set -- "$options"

while true
do
    case "$1" in
        -n|--name)      name=$2; shift 2;;
        -p|--path)      path=$2; shift 2;;
        -r|--rootfs)    rootfs=$2; shift 2;;
        --overlay)      overlay=$2; shift 2;;
        --mapped-uid)   mapped_uid=$2; shift 2;;
        --mapped-gid)   mapped_gid=$2; shift 2;;
        -h|--help)      usage $0 && exit 0;;
        --)             shift 1; break ;;
        *)              break ;;
    esac
done

if [ "$(id -u)" != "0" ]; then
    echo "This script should be run as 'root'"
    exit 1
fi

if [ -z "$name" ]; then
    echo "'name' parameter is required"
    exit 1
fi

if [ -z "$path" ]; then
    echo "'path' parameter is required"
    exit 1
fi

if [ -z "$rootfs" ]; then
    rootfs=$path/rootfs
fi

if [ -z "$overlay" ]; then
    echo "'overlay' parameter is required"
    exit 1
fi

if ! install $path; then
    echo "failed to install files"
    exit 1
fi

if ! configure $path $rootfs $name; then
    echo "failed to create configuration file"
    exit 1
fi
