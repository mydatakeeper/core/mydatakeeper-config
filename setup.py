#!/usr/bin/env python3

from setuptools import setup

with open('README.rst') as f:
    readme = f.read()

setup(
    name = "mydatakeeper-config",
    version = "0.0.1",
    description = "Mydatakeeper config internals",
    long_description = readme,
    author = "Théo Goudout",
    author_email = "theo.goudout@mydatakeeper.fr",
    url = "https://mydatakeeper.fr",
    keywords = "mydatakeeper",
    license = "GPLv3+",

    packages = ["mydatakeeper_config"],
    package_data = {"": ["LICENSE"]},
    package_dir = {"mydatakeeper_config": "mydatakeeper_config"},
    zip_safe = True,
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ]
)
